package com.java.org;

import java.util.Calendar;
import java.util.Map;
import java.util.TreeMap;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;
import com.java.org.*;

public class TagHandler extends TagSupport {
	
	
	public int doStartTag() throws JspException {  
		 
	    JspWriter out=pageContext.getOut();//returns the instance of JspWriter  
	    Treemap tree = new Treemap();
		   TreeMap<String,String> menumap=tree.property();
		pageContext.setAttribute("map", menumap);
		
	     
		return( SKIP_BODY );//will not evaluate the body content of the tag  
	} 

	
}
